#include <Wire.h>

#include "SparkFun_Qwiic_Scale_NAU7802_Arduino_Library.h" // Click here to get the library: http://librarymanager/All#SparkFun_NAU8702

#define LBF_TO_KILLO(x) (x * (8388608 / 4.53592))


NAU7802 myScale; //Create instance of the NAU7802 class
long min_value = 0;
long currentReading = 0;
const int buttonPin = A1; 

int buttonState = 0;         // variable for reading the pushbutton status
static void print_read_data(void);
static void convert_reading(void);
void(* resetFunc) (void) = 0; //declare reset function @ address 0

void setup() {

  Serial.begin(57600);
  while ( !Serial ) ;
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  digitalWrite(buttonPin, HIGH); 
  
  Wire.begin();
  if (myScale.begin() == false)
  {
    Serial.println("Scale not detected. Please check wiring. Freezing...");
    while (1);
  }
  Serial.println("Scale detected!");
  if(myScale.setLDO(3.3) == false)
  {
      Serial.println("LDO_NOT set!");
  }
  myScale.setGain(NAU7802_GAIN_2); //Gain can be set to 1, 2, 4, 8, 16, 32, 64, or 128.

  myScale.setSampleRate(NAU7802_SPS_20); //Sample rate can be set to 10, 20, 40, 80, or 320Hz

  myScale.calibrateAFE();
  min_value = 0;

}
//(8,388,608 / 4.53592)
static void convert_reading(void)
{
  (currentReading < 0) ? currentReading *= -1 : 0;
  currentReading = (currentReading - min_value >= 0)? (((currentReading - min_value))) : 0;
  currentReading = (currentReading);
}
static void print_read_data(void)
{
  Serial.print("<");
  Serial.print(currentReading);
  Serial.print(">");
}

void loop()
{
  buttonState = digitalRead(buttonPin);
  if (buttonState != HIGH)
  {
    Serial.println("buttonState == LOW");
    delay(1000);
    myScale.reset();
    resetFunc();
  } 
  if (myScale.available() == true)
  {
    if(min_value != 0)
    {
      currentReading = myScale.getReading();
      convert_reading();
      print_read_data(); 
    }
    else
    {
        min_value = myScale.getAverage(10); 
        (min_value < 0) ? min_value *= -1 : 0;
        Serial.print("ZERO_OFFSET == ");
        Serial.println(min_value); 
        delay(1000);
    }
  }
}
